import pytest

import savior

@pytest.fixture()
def book_db(tmpdir):
    path = str(tmpdir.join("test.lmdb"))
    with savior.open(path, ['users', 'books']) as db:
        yield db

def test_open_close_db_1(tmpdir):
    path = str(tmpdir.join("test.lmdb"))
    db = savior.open(path, ['a'])
    db.close()
    with pytest.raises(Exception) as e:
        db.begin()

def test_open_close_db_2(tmpdir):
    path = str(tmpdir.join("test.lmdb"))
    with savior.open(path, ['a']) as db:
        with db.begin(write=True) as txn:
            with pytest.raises(Exception) as e:
                txn.store('b', {'a': 1})

def test_database_storage_version(book_db):
    assert book_db.VERSION == 1

def test_create_tables(book_db):
    tables = book_db.tables
    assert tables['users']
    assert tables['books']

def test_store_fetch_1(book_db):
    entry = {'a': 'b', 'c': 'd'}
    with book_db.begin(write=True) as txn:
        id = txn.store('books', entry)
    with book_db.begin() as txn:
        book = txn.fetch('books', id)
        assert book == entry

def test_store_query_1(book_db):
    ids = {}
    with book_db.begin(write=True) as txn:
        for i in range(0, 20):
            uuid = txn.store('users', {'n': i, 'even': i % 2 == 0})
            ids[i] = uuid
    with book_db.begin() as txn:
        evens = txn.query('users', {'even': True})
        assert len(evens) == 10
        nines = txn.query('users', {'n': 9})
        assert nines[ids[9]]['n'] == 9

def test_store_query_2(book_db):
    with book_db.begin(write=True) as txn:
        for i in range(0, 20):
            txn.store('users', {'n': i, 'even': i % 2 == 0})
    with book_db.begin() as txn:
        all_users = txn.query('users', {})
        assert len(all_users) == 20

def test_store_query_3(book_db):
    with book_db.begin(write=True) as txn:
        for i in range(0, 10):
            txn.store('users', {'n': i, 'even': i % 2 == 0})
    with book_db.begin() as txn:
        all_users = txn.query('users', {})
        for user in all_users.values():
            assert 'created_at' not in user
        all_users = txn.query('users', {}, created_at=True)
        for user in all_users.values():
            assert 'created_at' in user

def test_store_update_1(book_db):
    ids = {}
    with book_db.begin(write=True) as txn:
        for i in range(0, 20):
            uuid = txn.store('users', {'n': i, 'even': i % 2 == 0})
            ids[i] = uuid
    with book_db.begin(write=True) as txn:
        txn.update('users', ids[6], {'n': 12, 'mod': True})
    with book_db.begin() as txn:
        u = txn.fetch('users', ids[6])
        assert u == {'n': 12, 'even': True, 'mod': True}

def test_manual_abort_1(book_db):
    txn = book_db.begin(write=True)
    uuid = txn.store('books', {'name': 'moby'})
    assert txn.fetch('books', uuid)
    txn.abort()
    with book_db.begin() as txn:
        assert txn.fetch('books', uuid) == {}
    
def test_manual_commit_1(book_db):
    txn = book_db.begin(write=True)
    uuid = txn.store('books', {'name': 'moby'})
    assert txn.fetch('books', uuid)
    txn.commit()
    with book_db.begin() as txn:
        assert txn.fetch('books', uuid) == {'name': 'moby'}
    
def test_delete_1(book_db):
    ids = {}
    with book_db.begin(write=True) as txn:
        for i in range(0, 20):
            uuid = txn.store('users', {'n': i, 'even': i % 2 == 0})
            ids[i] = uuid
        txn.update('users', ids[9], {'mod': True})
    with book_db.begin(write=True) as txn:
        txn.delete('users', ids[9])
    with book_db.begin() as txn:
        nines = txn.query('users', {'n': 9})
        assert nines == {}
        odds = txn.query('users', {'even': False})
        evens = txn.query('users', {'even': True})
        assert len(odds) == 9
        assert len(evens) == 10

